<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>";
echo "Legs : " . $sheep->legs; // 4
echo "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded; // "no"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name;
echo "<br>";
echo "Legs : " . $kodok->legs;
echo "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded;
echo "<br>";
echo "Jump : ";
$kodok->jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name;
echo "<br>";
echo "Legs : " . $sungokong->legs;
echo "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded;
echo "<br>";
echo "Yell : ";
$sungokong->yell();
echo "<br><br>";
